// =============== LIBRARY
import {
  ReminderApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import { RMD_SEND, RMD_VALIDATE, RMD_UPLOAD, RMD_DOCS } from "../action-types";
import { RMD_SET_DOCS, RMD_SET_ERROR, RMD_RESET } from "../mutation-types";

// =============== STATE
const state = {
  retireDocs: null,
  promoteDocs: null,
  error: null
};

// =============== GETTERS
const getters = {
  retireDocs(state) {
    return state.retireDocs;
  },
  promoteDocs(state) {
    return state.promoteDocs;
  }
};

// =============== ACTIONS
const actions = {
  [RMD_SEND](context, payload) {
    return new Promise((resolve, reject) => {
      ReminderApiService.send({
        nip: payload.nip,
        type: payload.type
      })
        .then(({ data }) => {
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: "Reminder Berhasil Dikirim.",
            text: "Reminder untuk " + payload.nip + " berhasil dikirim.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(RMD_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [RMD_VALIDATE](context, payload) {
    return new Promise((resolve, reject) => {
      ReminderApiService.validate({
        nip: payload.nip,
        type: payload.type,
        date_promotion: payload.validate.date_promotion
      })
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Validasi Berhasil.",
            text: "Validasi untuk " + payload.nip + " telah berhasil.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(RMD_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [RMD_UPLOAD](context, payload) {
    return new Promise((resolve, reject) => {
      ReminderApiService.upload(payload.file)
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Berhasil.",
            text: "File berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(RMD_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [RMD_DOCS](context, payload) {
    return new Promise((resolve, reject) => {
      ReminderApiService.get({
        nip: payload.nip
      })
        .then(({ data }) => {
          context.commit(RMD_SET_DOCS, {
            documents: data.values.documents,
            type: 1
          });
          if (payload.closeLoading) payload.closeLoading();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(RMD_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  }
};

// =============== MUTATIONS
const mutations = {
  [RMD_SET_DOCS](state, payload) {
    if (payload.type == 1) {
      state.promoteDocs = payload.documents;
    } else if (payload.type == 2) {
      state.retireDocs = payload.documents;
    }
  },
  [RMD_SET_ERROR](state, error) {
    state.error = error;
  },
  [RMD_RESET](state) {
    state.promoteDocs = null;
    state.retireDocs = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
