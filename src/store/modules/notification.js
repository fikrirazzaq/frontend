// =============== LIBRARY
import {
  NotificationApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import { NTF_FETCH_ALL, NTF_SELECT } from "../action-types";
import {
  NTF_SET_NOTIFICATIONS,
  NTF_SET_SELECTED,
  NTF_SET_UNREAD_COUNT,
  NTF_SET_READ,
  NTF_RESET,
  NTF_SET_ERROR
} from "../mutation-types";

// =============== STATE
const state = {
  notifications: null,
  unreadNotificationCount: 0,
  selectedNotif: null,
  error: null
};

// =============== GETTERS
const getters = {
  notifications(state) {
    return state.notifications;
  },
  openedNotifications(state) {
    if (!!state.notifications)
      return state.notifications.filter(notif => notif.read.read_status);
    else return [];
  },
  unreadNotifications(state) {
    if (!!state.notifications)
      return state.notifications.filter(notif => !notif.read.read_status);
    else return [];
  },
  unreadNotificationCount(state) {
    return state.unreadNotificationCount;
  },
  selectedNotif(state) {
    return state.selectedNotif;
  }
};

// =============== ACTIONS
const actions = {
  [NTF_FETCH_ALL](context, payload) {
    return new Promise((resolve, reject) => {
      NotificationApiService.get(payload.role)
        .then(({ data }) => {
          context.commit(NTF_SET_NOTIFICATIONS, data.values);
          const unread = state.notifications.filter(
            notif => !notif.read.read_status
          );
          context.commit(NTF_SET_UNREAD_COUNT, unread.length);
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(NTF_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [NTF_SELECT](context, payload) {
    return new Promise((resolve, reject) => {
      NotificationApiService.read(payload.reader.role, {
        notif_id: payload.notif_id
      })
        .then(({ data }) => {
          context.commit(NTF_SET_READ, {
            reader: payload.reader,
            notif_id: payload.notif_id
          });
          let notif_index = state.notifications.findIndex(
            notif => notif.notif_id == payload.notif_id
          );
          context.commit(NTF_SET_SELECTED, state.notifications[notif_index]);
          const unread = state.notifications.filter(
            notif => !notif.read.read_status
          );
          context.commit(NTF_SET_UNREAD_COUNT, unread.length);
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(NTF_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  }
};

// =============== MUTATIONS
const mutations = {
  [NTF_SET_NOTIFICATIONS](state, notifications) {
    state.notifications = notifications;
  },
  [NTF_SET_SELECTED](state, notif) {
    state.selectedNotif = notif;
  },
  [NTF_SET_UNREAD_COUNT](state, count) {
    state.unreadNotificationCount = count;
  },
  [NTF_SET_READ](state, payload) {
    let notif_index = state.notifications.findIndex(
      notif => notif.notif_id == payload.notif_id
    );
    state.notifications[notif_index].read.read_status =
      payload.reader.read_status;
    state.notifications[notif_index].read.read_time = new Date(Date.now());
    if (state.userDisplayRole == "admin") {
      state.notifications[notif_index].payload.reader.name =
        payload.reader.name;
      state.notifications[notif_index].payload.reader.nip = payload.reader.nip;
      state.notifications[notif_index].payload.reader.email =
        payload.reader.email;
    }
  },
  [NTF_SET_ERROR](state, error) {
    state.error = error;
  },
  [NTF_RESET](state) {
    state.notifications = null;
    state.unreadNotificationCount = 0;
    state.selectedNotif = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
