// =============== LIBRARY
import {
  ProfileApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import { PROFILE_FETCH, PROFILE_UPLOAD, PROFILE_DOCS } from "../action-types";
import {
  PROFILE_SET,
  PROFILE_SET_DOCS,
  PROFILE_SET_ERROR,
  PROFILE_RESET,
  AUTH_SET_USER_PICTURE
} from "../mutation-types";

// =============== STATE
const state = {
  profile: null,
  profileDocs: null,
  error: null
};

// =============== GETTERS
const getters = {
  profile(state) {
    return state.profile;
  },
  profileDocs(state) {
    return state.profileDocs;
  }
};

// =============== ACTIONS
const actions = {
  [PROFILE_FETCH](context, payload) {
    return new Promise((resolve, reject) => {
      ProfileApiService.get()
        .then(({ data }) => {
          context.commit(PROFILE_SET, data.values);
          const docs = [
            { title: data.values.kk, type: "kk", url: data.values.kk },
            { title: data.values.ktp, type: "ktp", url: data.values.ktp },
            { title: data.values.npwp, type: "npwp", url: data.values.npwp },
            {
              title: data.values.ijazah,
              type: "ijazah",
              url: data.values.ijazah
            },
            {
              title: data.values.kartu_pegawai,
              type: "kartu_pegawai",
              url: data.values.kartu_pegawai
            }
          ];
          context.commit(PROFILE_SET_DOCS, docs);
          context.commit(AUTH_SET_USER_PICTURE, data.values.picture);
          if (payload.closeLoading) payload.closeLoading();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(PROFILE_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [PROFILE_UPLOAD](context, payload) {
    return new Promise((resolve, reject) => {
      ProfileApiService.upload(payload.file)
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Berhasil.",
            text: "File berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(PROFILE_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [PROFILE_DOCS](context, payload) {}
};

// =============== MUTATIONS
const mutations = {
  [PROFILE_SET](state, profile) {
    state.profile = profile;
  },
  [PROFILE_SET_DOCS](state, profileDocs) {
    state.profileDocs = profileDocs;
  },
  [PROFILE_SET_ERROR](state, error) {
    state.error = error;
  },
  [PROFILE_RESET](state) {
    state.profile = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
