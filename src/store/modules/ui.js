// =============== LIBRARY
import {
  UI_UPDATE_SIDEBAR_WIDTH,
  UI_UPDATE_WINDOW_WIDTH,
  UI_TOGGLE_CONTENT_OVERLAY
} from "../action-types";
import {
  UI_SET_SIDEBAR_WIDTH,
  UI_SET_SIDEBAR_ITEMS_MIN,
  UI_SET_WINDOW_WIDTH,
  UI_SET_WINDOW_BREAKPOINT,
  UI_IS_REDUCE_BUTTON,
  UI_IS_CONTENT_OVERLAY,
  UI_IS_SIDEBAR_ACTIVE
} from "../mutation-types";

// =============== STATE
const state = {
  isSidebarActive: true,
  breakpoint: null,
  sidebarWidth: "default",
  reduceButton: false,
  bodyOverlay: false,
  sidebarItemsMin: false,
  themePrimaryColor: "#495678",
  windowWidth: null
};

// =============== GETTERS
const getters = {
  themePrimaryColor(state) {
    return state.themePrimaryColor;
  },
  bodyOverlay(state) {
    return state.bodyOverlay;
  },
  sidebarWidth(state) {
    return state.sidebarWidth;
  },
  breakpoint(state) {
    return state.breakpoint;
  },
  sidebarItemsMin(state) {
    return state.sidebarItemsMin;
  },
  isSidebarActive(state) {
    return state.isSidebarActive;
  }
};

// =============== ACTIONS
const actions = {
  [UI_UPDATE_SIDEBAR_WIDTH](context, payload) {
    context.commit(UI_SET_SIDEBAR_WIDTH, payload);
  },
  [UI_UPDATE_WINDOW_WIDTH](context, payload) {
    context.commit(UI_SET_WINDOW_WIDTH, payload);
  },
  [UI_TOGGLE_CONTENT_OVERLAY](context) {
    context.commit(UI_IS_CONTENT_OVERLAY);
  }
};

// =============== MUTATIONS
const mutations = {
  [UI_SET_SIDEBAR_WIDTH](state, width) {
    state.sidebarWidth = width;
  },
  [UI_SET_SIDEBAR_ITEMS_MIN](state, value) {
    state.sidebarItemsMin = value;
  },
  [UI_SET_WINDOW_BREAKPOINT](state, value) {
    state.breakpoint = value;
  },
  [UI_SET_WINDOW_WIDTH](state, width) {
    state.windowWidth = width;
  },
  [UI_IS_REDUCE_BUTTON](state, value) {
    state.reduceButton = value;
  },
  [UI_IS_CONTENT_OVERLAY](state, value) {
    state.bodyOverlay = value;
  },
  [UI_IS_SIDEBAR_ACTIVE](state, value) {
    state.isSidebarActive = value;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
