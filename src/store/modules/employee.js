// =============== LIBRARY
import {
  EmployeeApiService,
  ProfileApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import {
  EMP_FETCH,
  EMP_FETCH_ALL,
  EMP_ADD,
  EMP_EDIT,
  EMP_DELETE,
  EMP_UPLOAD,
  EMP_DOCS
} from "../action-types";
import {
  EMP_SET_EMPLOYEES,
  EMP_SET_SELECTED,
  EMP_SET_SELECTED_SINGLE,
  EMP_SET_DOCS,
  EMP_SET_ERROR,
  EMP_RESET
} from "../mutation-types";
import decoder from "../../utils/decoder";

// =============== STATE
const state = {
  employees: null,
  selectedEmployee: null,
  employeeDocs: null,
  error: null
};

// =============== GETTERS
const getters = {
  employees(state) {
    return state.employees;
  },
  selectedEmployee(state) {
    return state.selectedEmployee;
  },
  employeeDocs(state) {
    return state.employeeDocs;
  }
};

// =============== ACTIONS
const actions = {
  [EMP_FETCH_ALL](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.getall()
        .then(({ data }) => {
          context.commit(EMP_SET_EMPLOYEES, data.values);
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_FETCH](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.get({
        nip: payload.employee.nip
      })
        .then(({ data }) => {
          context.commit(EMP_SET_SELECTED, data.values);
          if (payload.closeLoading) payload.closeLoading();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_RESET);
          context.commit(EMP_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_ADD](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.add(payload.user_form)
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Data Berhasil Disimpan.",
            text:
              "Data " + payload.user_form.first_name + " berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_EDIT](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.edit(payload.user_form)
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Data Berhasil Disimpan.",
            text:
              "Data " + payload.user_form.first_name + " berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          console.log(response);
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_DELETE](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.delete({
        nip: payload.employee.nip
      })
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Berhasil.",
            text: "Data " + payload.employee.name + " berhasil dihapus.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_UPLOAD](context, payload) {
    return new Promise((resolve, reject) => {
      ProfileApiService.upload(payload)
        .then(({ data }) => {
          resolve(data);
        })
        .catch(({ response }) => {
          reject(response);
        });
    });
  },
  [EMP_DOCS](context, payload) {}
};

// =============== MUTATIONS
const mutations = {
  [EMP_SET_EMPLOYEES](state, employees) {
    state.employees = employees;
  },
  [EMP_SET_SELECTED](state, employee) {
    state.selectedEmployee = employee;
  },
  [EMP_SET_SELECTED_SINGLE](state, payload) {
    state.selectedEmployee[payload.type] = payload.value;
  },
  [EMP_SET_DOCS](state, employeeDocs) {
    state.employeeDocs = employeeDocs;
  },
  [EMP_SET_ERROR](state, error) {
    state.error = error;
  },
  [EMP_RESET](state) {
    state.employees = null;
    state.selectedEmployee = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
