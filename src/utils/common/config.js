export const API_URL = `http://${process.env.VUE_APP_API_BACKEND_HOST}:${process.env.VUE_APP_API_BACKEND_PORT}`;
export const DEVELOP_URL = process.env.URL_DEVELOP;
export default API_URL;
